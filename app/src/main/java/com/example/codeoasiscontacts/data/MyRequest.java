package com.example.codeoasiscontacts.data;

import java.util.List;

/**
 * Created by i3 on 03.05.2017.
 */

public class MyRequest {

    private List<MyContact> contacts = null;

    public List<MyContact> getContacts() {
        return contacts;
    }

    public void setContacts(List<MyContact> contacts) {
        this.contacts = contacts;
    }

    @Override
    public String toString() {
        String res = "";
        for (MyContact mc : contacts)
            res += mc.getId() + " " + mc.getName() + " *** ";
        return res;
    }
}
