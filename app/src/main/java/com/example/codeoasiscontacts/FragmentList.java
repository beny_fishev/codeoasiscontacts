package com.example.codeoasiscontacts;

import android.app.Fragment;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Canvas;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.codeoasiscontacts.data.MyContact;
import com.example.codeoasiscontacts.data.MyPhone;
import com.example.codeoasiscontacts.utils.DBHelper;
import com.example.codeoasiscontacts.utils.MyRecAdaper;

import java.util.ArrayList;

public class FragmentList extends Fragment {

    private DBHelper dbHelper;
    private MyRecAdaper adapter;
    private RecyclerView rView;
    private ArrayList<MyContact> contacts2delete = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_fragment, null);

        adapter = new MyRecAdaper(fillAdapter());
        rView = (RecyclerView) view.findViewById(R.id.rec_view_holder);
        rView.setLayoutManager(new LinearLayoutManager(container.getContext()));
        rView.setItemAnimator(new DefaultItemAnimator());
        rView.addItemDecoration(new DividerItemDecoration(container.getContext(), LinearLayoutManager.HORIZONTAL));
        rView.setAdapter(adapter);

        initSwipe();

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    private void initSwipe() {
        //  attaching swipe gesture to the Recycler view
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                MyContact contact = adapter.getContact(position);
                Log.d("TEST_LIST", "on Swipe delete contact " + contact.getId() + " " + contact.getName());
                contacts2delete.add(adapter.getContact(position));
                adapter.removeItem(position);
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                                    float dX, float dY, int actionState, boolean isCurrentlyActive) {
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(rView);
    }

    @Override
    public void onPause() {
        super.onPause();
        delFromDb(contacts2delete);
    }

    private void delFromDb(final ArrayList<MyContact> cont2del) {
        dbHelper = new DBHelper(getActivity());
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (MyContact mc : cont2del) {
                    Log.d("TEST_LIST", "deleting from DB " + mc.getId() + " " + mc.getName());
                    dbHelper.getWritableDatabase().delete(DBHelper.TABLE, DBHelper.COL_ID + "=?", new String[]{String.valueOf(mc.getId())});
                }
            }
        }).start();
    }

    private ArrayList<MyContact> fillAdapter() {
        //  Creating ArrayList of contacts from DB
        ArrayList<MyContact> res = new ArrayList<>();
        Log.d("TEST_FILL_ADAPTER", "Fill adapter 1");
        dbHelper = new DBHelper(getActivity());
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor c = db.rawQuery("select * from " + dbHelper.TABLE, null);
        if (c.moveToFirst()) {
            do {
                res.add(new MyContact(c.getString(0), c.getString(1), c.getString(2), c.getString(3), c.getString(4),
                        new MyPhone(c.getString(5), c.getString(6), c.getString(7))));
            } while (c.moveToNext());
        }
        return res;
    }
}
