package com.example.codeoasiscontacts;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.codeoasiscontacts.data.MyContact;
import com.example.codeoasiscontacts.data.MyRequest;
import com.example.codeoasiscontacts.utils.DBHelper;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    final Handler handler = new Handler();
    final DBHelper dbHelper = new DBHelper(this);

    private static AlertDialog.Builder alertDialog;
    private Timer timer;
    private Boolean readyFlag = false;
    static FragmentManager manager;
    static int counter = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (alertDialog == null) {
            alertDialog = new AlertDialog.Builder(this);
        }
        manager = getFragmentManager();
        Fragment fragment = null;
        fragment = manager.findFragmentByTag("FR_LIST");
        if (fragment == null && counter == 5){
            setSplashScreen();
            myOkHttpRequest();
            setMyTimer();
        }

    }

    private void setSplashScreen(){
        FragmentTransaction trans = manager.beginTransaction();
        trans.add(R.id.frag_container, new FragmentSplash(), "FR_SPALSH");
        trans.commit();
    }

    private void setMyTimer(){ //waiting for 2 seconds and parsing request data
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (readyFlag) {
                    // Starting list Fragment
                    timer.cancel();
                    Log.d("TEST", "Timer cancel");
                    FragmentTransaction trans = manager.beginTransaction();
                    Fragment fr = manager.findFragmentByTag("FR_SPALSH");
                    trans.remove(fr);
                    trans.add(R.id.frag_container, new FragmentList(), "FR_LIST");
                    trans.commit();
                }else if (counter > 0) {
                    counter--;
                } else {
                    // Starting alertDialog for Repeat request / Exit app
                    timer.cancel();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            initDialog();
                            alertDialog.show();
                        }
                    });
                }
            }
        }, 2000, 1000);
    }

    private void initDialog(){
        alertDialog.setTitle("Server not responding");
        alertDialog.setMessage("Repeat request / Exit");
        alertDialog.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                finish();
            }
        });
        alertDialog.setNegativeButton("Repeat", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                //  Next 5 second period for request / parsing
                counter = 5;
                myOkHttpRequest();
                setMyTimer();
            }
        });
        alertDialog.setCancelable(false);
    }

    private void myOkHttpRequest() {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url("http://api.androidhive.info/contacts/")
                .get()
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("TEST", "Server not responding");
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                final String respBody = response.body().string();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (response.isSuccessful()) {
                            MyRequest mReq = (new Gson()).fromJson(respBody, MyRequest.class);

                            Collections.sort(mReq.getContacts(), new Comparator<MyContact>() {
                                @Override
                                public int compare(MyContact o1, MyContact o2) {
                                    return o1.getName().compareTo(o2.getName());
                                }
                            });

                            clearDB();
                            for (MyContact mc : mReq.getContacts()) {
                                addData2Db(mc);
                            }
                            //  request Ok, can start new fragment
                            readyFlag = true;
                        }
                    }
                });
            }
        });
    }

    public void clearDB() {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.delete(DBHelper.TABLE, null, null);
    }

    public void addData2Db(MyContact mc) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put(DBHelper.COL_ID, mc.getId());
        cv.put(DBHelper.COL_NAME, mc.getName());
        cv.put(DBHelper.COL_EML, mc.getEmail());
        cv.put(DBHelper.COL_ADR, mc.getAddress());
        cv.put(DBHelper.COL_GEN, mc.getGender());
        cv.put(DBHelper.COL_PH_MOB, mc.getPhone().getMobile());
        cv.put(DBHelper.COL_PH_HOME, mc.getPhone().getHome());
        cv.put(DBHelper.COL_PH_OFF, mc.getPhone().getOffice());

        db.insert(DBHelper.TABLE, null, cv);
        db.close();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dbHelper.close();
    }
}