package com.example.codeoasiscontacts.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.codeoasiscontacts.R;
import com.example.codeoasiscontacts.data.MyContact;

import java.util.ArrayList;
import java.util.List;

public class MyRecAdaper  extends RecyclerView.Adapter<MyRecAdaper.MyViewHolder>  {

    private List<MyContact> contList = new ArrayList<MyContact>();
    private Context context;

    public MyRecAdaper() {}

    public MyRecAdaper(List<MyContact> contList) {
        this.contList = contList;
    }

    public void removeItem(int position){
        contList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, contList.size());
    }

    public MyContact getContact(int position){
        return contList.get(position);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item, parent,false);
        context = parent.getContext();
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final MyContact mc = contList.get(position);
        holder.txtName.setText(mc.getName());
        holder.txtPhone.setText(mc.getPhone().getMobile());
        holder.ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callOnClick(mc.getPhone().getMobile());
            }
        });
    }

    @Override
    public int getItemCount() {
        return contList.size();
    }

    public void callOnClick(String phone) {
        Intent dial = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+phone));
        context.startActivity(dial);
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        TextView txtName, txtPhone;
        LinearLayout ll;
        public MyViewHolder(View v) {
            super(v);
            txtName = (TextView) v.findViewById(R.id.txt_item_name);
            txtPhone = (TextView) v.findViewById(R.id.txt_item_phone);
            ll = (LinearLayout) v.findViewById(R.id.item_layout);
        }
    }
}
