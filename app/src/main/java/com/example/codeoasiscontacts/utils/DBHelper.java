package com.example.codeoasiscontacts.utils;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class DBHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "contacts.db";
    private static final int SCHEMA = 1;
    public static final String TABLE = "contacts";
    //Column names
    public static final String COL_ID = "id";
    public static final String COL_NAME = "name";
    public static final String COL_EML = "email";
    public static final String COL_ADR = "address";
    public static final String COL_GEN = "gender";
    public static final String COL_PH_MOB = "ph_mob";
    public static final String COL_PH_HOME = "ph_home";
    public static final String COL_PH_OFF = "ph_off";

    public DBHelper(Context context) {
        super(context, DB_NAME, null, SCHEMA);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d("HELPER", " on create");

        db.execSQL("CREATE TABLE "+ TABLE + "("+
                COL_ID+" TEXT, "+
                COL_NAME+" TEXT, "+
                COL_EML+" TEXT, "+
                COL_ADR+" TEXT, "+
                COL_GEN+" TEXT, "+
                COL_PH_MOB+" TEXT, "+
                COL_PH_HOME+" TEXT, "+
                COL_PH_OFF+" TEXT)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.delete(TABLE, null, null);
        db.execSQL("DROP TABLE " + TABLE);
        onCreate(db);
    }
}
